# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy


class DoubanDataItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()

    # 列表页，每条电影数据
    episodes_info = scrapy.Field()
    # 评分
    rate = scrapy.Field()
    cover_x = scrapy.Field()
    # 电影名字
    title = scrapy.Field()
    # 电影详情页面链接
    url = scrapy.Field()
    playable = scrapy.Field()
    # 背景图片
    cover_img = scrapy.Field()
    # 背景图片 url
    cover = scrapy.Field()
    id = scrapy.Field()
    cover_y = scrapy.Field()
    # 是否是最新电影
    is_new = scrapy.Field()

    # 详情页
    # 导演
    directed_by = scrapy.Field()
    # 编剧
    screenwriter = scrapy.Field()
    # 主演
    actor = scrapy.Field()
    # 类型
    type = scrapy.Field()
    # 国家
    country = scrapy.Field()
    # 语言
    language = scrapy.Field()
    # 上映日期
    release_date = scrapy.Field()
    # 片长
    runtime = scrapy.Field()
    # 又名
    nickname = scrapy.Field()
    IMDb = scrapy.Field()
    # 评价人数
    rating_people = scrapy.Field()


    pass
