import scrapy
from douban_data.items import DoubanDataItem
import json
from imp import reload

class DoubanComSpider(scrapy.Spider):
    name = 'douban_com'
    allowed_domains = ['douban.com', 'doubanio.com']
    start_urls = ['http://douban.com/']

    def start_requests(self):
        reqs = []
        for i in range(1, 3):
            url = "https://movie.douban.com/j/search_subjects?type=movie&tag=热门&sort=recommend&page_limit=20&page_start=%s"
            req = scrapy.Request(url % i)
            print(req)
            reqs.append(req)
        return reqs

    def parse(self, response):
        film_list = json.loads(response.text)['subjects']
        print(film_list)
        for film in film_list[0:]:
            film_item = DoubanDataItem()
            film_item['episodes_info'] = film['episodes_info']
            film_item['rate'] = film['rate']
            film_item['cover_x'] = film['cover_x']
            film_item['title'] = film['title']
            film_item['url'] = film['url']
            film_item['playable'] = film['playable']
            film_item['cover_img'] = [film['cover']]
            film_item['cover'] = film['cover']
            film_item['id'] = film['id']
            film_item['cover_y'] = film['cover_y']
            film_item['is_new'] = film['is_new']

            yield scrapy.Request(url=film_item['url'], meta={'film_item': film_item},
                                 callback=self.parse_detail,
                                 dont_filter=True)

    def parse_detail(self, response):
        film_item = response.meta['film_item']
        film_item['directed_by'] = response.xpath('//*[@id="info"]/span[1]/span[2]/a/text()')[0].extract()

        yield film_item





